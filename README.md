# dotfiles

## .aliases

A list of aliases for bash.

## .gitconfig

Global configuration for Git.

## .gitignore

Global gitignore file for Git (to be referenced in global .gitconfig file).

## .vimrc

Global configuration for VIm.

## brew (MacOS)

After installing [brew](https://brew.sh/), see [homebrew-file](https://github.com/rcmdnk/homebrew-file)