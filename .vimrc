set nocompatible
syntax on
filetype plugin indent on
set mouse=a
set number
set laststatus=2 ignorecase smartcase hlsearch incsearch

" show existing tab with 4 spaces width
set tabstop=4
" soft tab width
set softtabstop=4
set shiftwidth=4
" 4 spaces tab
set expandtab

autocmd BufNewFile,BufRead *.ezt set filetype=html

" Vanilla key remapping PREVIOUS - NEXT
:nnoremap <C-n> :bnext<CR>
:nnoremap <C-p> :bprevious<CR>

" indentation made from visual mode
set smartindent
